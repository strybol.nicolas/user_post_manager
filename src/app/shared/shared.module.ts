import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { FlexLayoutModule } from '@angular/flex-layout';

import { NgZorroAntdModule } from 'ng-zorro-antd';

import { PipesModule } from '@app/core/pipes';

const MODULES = [
    CommonModule,
    FormsModule,
    FlexLayoutModule,
    NgZorroAntdModule,
    PipesModule
];

@NgModule({
    declarations: [],
    imports: [
        ...MODULES
    ],
    exports: [
        ...MODULES
    ]
})
export class SharedModule { }
