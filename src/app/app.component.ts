import { Component } from '@angular/core';

import { NzIconService } from 'ng-zorro-antd';

import { ICONS } from 'src/mdi-icons';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent { 

    isCollapsed = false;

	configs: any;

	constructor(
        private iconService: NzIconService,
	) {
        this.iconService.addIcon(...ICONS);	// Loading mdi Icons, well just 2
    }

	ngOnInit() { }

}
