import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PostComponent } from '@app/pages/post'

const routes: Routes = [
    { path: 'users', loadChildren: () => import('./pages/users/users.module').then(module => module.UsersModule) },
    { path: 'post/:id', component: PostComponent },
    { path: '', pathMatch: 'full', redirectTo: '/users' },
    { path: '**', redirectTo: '/users' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
