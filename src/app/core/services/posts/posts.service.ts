import { Injectable } from '@angular/core';

import { ApiService } from '@core/services/api';

@Injectable({
    providedIn: 'root'
})
export class PostsService {

    url: string = "posts";

    constructor(
        private apiService: ApiService
    ) { }


    /**
     * Retrieve all Posts
     */
    getPosts() {
        return this.apiService.getRequest(this.url);
    }

    /**
     * Retrieve all posts by user.id(userId for Post)
     * @param userId 
     */
    getPostsByUserId(userId: number) {
        return this.apiService.getRequestByUserId(this.url, userId);
    }

    /**
     * Retrieve a post by its id
     * @param id 
     */
    getPostById(id: number) {
        return this.apiService.getRequestById(this.url, id);
    }
}
