import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class ApiService {

    apiUrl = "https://jsonplaceholder.typicode.com/";

    constructor(
        private http: HttpClient
    ) { }

	
    /**
     * Send GET request to the specified URL and return response to calling methode
     * @param url 
     */
    getRequest(url: string) {
        return this.http.get(this.apiUrl + url);
    }

    /**
     * Send GET request to the specified URL and return retrieved documents, where userId is the user's id
     * @param url 
     */
    getRequestByUserId(url: string, userId: number) {
        return this.http.get(this.apiUrl + url + "?userId=" + userId);
    }

    /**
     * Send GET request to the specified URL and return single post, where id is the post's id
     * @param url 
     */
    getRequestById(url: string, id: number) {
        return this.http.get(this.apiUrl + url + "/" + id);
    }
}
