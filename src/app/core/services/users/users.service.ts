import { Injectable } from '@angular/core';

import { ApiService } from '@core/services/api';

@Injectable({
    providedIn: 'root'
})
export class UsersService {

    url: string = "users";

    constructor(
        private apiService: ApiService
    ) { }

    
    /**
     * Get all users
     */
    getUsers() {
        return this.apiService.getRequest(this.url);
    }

}
