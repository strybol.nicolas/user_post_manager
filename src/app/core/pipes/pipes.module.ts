import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// pipes
import { NameFilterPipe } from './nameFilter';

@NgModule({
    declarations: [
        NameFilterPipe
    ],
    imports: [CommonModule],
    exports: [
        NameFilterPipe
    ],
})
export class PipesModule {}