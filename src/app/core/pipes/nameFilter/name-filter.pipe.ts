import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'nameFilter'
})
export class NameFilterPipe implements PipeTransform {

    transform(list: any[], searchText: string): any {
        
        if (!list) { return []; }

        return list.filter(
            item => item.name.toLowerCase().includes(searchText.toLowerCase())
        );
    }

}
