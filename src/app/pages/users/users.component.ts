import { Component, OnInit } from '@angular/core';

import { Router } from "@angular/router";

import { UsersService } from '@core/services/users';
import { PostsService } from '@core/services/posts';

@Component({
    selector: 'app-users',
    templateUrl: './users.component.html',
    styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

    /** Table Vars **/
    users;

    selectedUser = {};

    searchInput: string = '';

    isTableLoading: boolean = true;

    /** Modal Vars **/
    posts;

    isVisible: boolean = false;
    isModalLoading: boolean = true;


    constructor(
        private router: Router,
        private usersService: UsersService,
        private postsService: PostsService
    ) { 
        this.usersService.getUsers()
            .toPromise()
            .then(
                element => {

                    this.isTableLoading = false;
                    this.users = element;        
                }
        );  
    }

    ngOnInit() { }

    
    /**
     * Send user to /post/:id, where :id represents the post's id.
     * @param selectedPost 
     */
    navigateToSelectedPost(selectedPost) { this.router.navigate(['/post/' + selectedPost.id]); }


    /** Filter **/

    /**
     * Sets the searchInput, used for searching, to empty string.
     */
    reset(): void {
        this.searchInput = '';
    }


    /** Modal Methods **/

    /**
     * Reveals modal on user selection in the user table
     * @param user 
     */
    showModal(user) {
        
        this.selectedUser = user;

        this.prepModal(user);

        this.isVisible = !this.isVisible;
    }


    /**
     * On user input, retrieve all posts for the user's selected user
     * @param user 
     */
    prepModal(user) {

        this.postsService.getPostsByUserId(user.id)
            .toPromise()
            .then(
                element => {
                    console.log("element =")
                    this.posts = element;
                    this.isModalLoading = false;
        });
    }

    /**
     * Hides modal, when the X symbol has been pressed in the top-right corner,
     * and set selectedUser to empty
     */
    onCancel() {
        this.isVisible = false;

        this.selectedUser = {};
    }
}
