import { NgModule } from '@angular/core';

import { SharedModule } from '@app/shared';

import { UsersRoutingModule } from './users-routing.module';

import { UsersComponent } from './users.component';

const COMPONENTS = [
    UsersComponent
];

@NgModule({
    declarations: [
        ...COMPONENTS
    ],
    imports: [
        SharedModule,
        UsersRoutingModule,
    ],
    exports: [
        ...COMPONENTS
    ]
})
export class UsersModule { }
