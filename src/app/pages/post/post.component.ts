import { Component, OnInit } from '@angular/core';

import { ActivatedRoute } from "@angular/router";

import { PostsService } from '@core/services/posts';

@Component({
    selector: 'app-post',
    templateUrl: './post.component.html',
    styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit {

    post;

    isLoading: boolean = false;

    constructor(
        private route: ActivatedRoute,
        private postsService: PostsService
    ) {
        
        this.route.params.subscribe(
            params => {

                // Get post with params.id, retrieved from route
                this.postsService.getPostById(params.id)
                    .toPromise()
                    .then(
                        element => {
                            
                            this.post = element;
                            this.isLoading = false;
                });
            });
            
    }

    ngOnInit() { }

}
