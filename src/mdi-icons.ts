
// Custom icon static resources
import { IconDefinition } from '@ant-design/icons-angular';

import {
    /** M **/
    mdiMagnify,
    mdiMessageOutline
} from '@mdi/js';


const svg = '<svg viewBox="0 0 24 24">\n ';


/** M **/
const mdiMagnifyIcon: IconDefinition = {
    name: 'mdi:magnify',
    icon: svg + '<path d="' + mdiMagnify + '" /> \n</svg>',
};
const mdiMessageOutlineIcon: IconDefinition = {
    name: 'mdi:message-outline',
    icon: svg + '<path d="' + mdiMessageOutline + '" /> \n</svg>',
};

export const ICONS = [
    /** M **/
    mdiMagnifyIcon,
    mdiMessageOutlineIcon
];
